def get_input():
    Name = input("Enter the name: ")
    Description = input("Enter the description: ")
    Secgroup = input("Enter the security group: ")
    Size = input("Enter the size: ")
    return Name, Description, Secgroup, Size

def write_to_file(Name, Description, Secgroup, Size):
    with open('{}.txt'.format(Name), 'w') as f:
        f.write("qq smb_add_share --name {} --fs-path /nonreplicated/{} --description {} --access-based-enumeration-enabled true --create-fs-path --require-encryption true --all-access\n".format(Name, Name, Description))
        f.write("qq quota_create_quota --path /nonreplicated/{} --limit {}\n".format(Name, Size))
        f.write("qq fs_modify_acl --path /nonreplicated/{} add_entry -t campusad\\\\{} -y Allowed -r Delete, Delete child, Execute/Traverse, Read, Take ownership, Write ACL, Write directory -f 'Container inherit' 'Object inherit'\n".format(Name, Secgroup))
        f.write("qq fs_modify_acl --path /nonreplicated/{} remove_entry --position 3\n".format(Name))
        f.write("qq fs_modify_acl --path /nonreplicated/{} remove_entry --position 2\n".format(Name))
        f.write("qq fs_modify_acl --path /nonreplicated/{} remove_entry --position 1\n".format(Name))
        f.write("\n")

def main():
    Name, Description, Secgroup, Size = get_input()
    write_to_file(Name, Description, Secgroup, Size)

if __name__ == "__main__":
    main()