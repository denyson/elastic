def get_input():
    ExportPath = input("Enter the export path: ")
    Description = input("Enter the description: ")
    Hosts = input("Enter the hosts: ")
    Size = input("Enter the size: ")
    return ExportPath, Description, Hosts, Size

def write_to_file(ExportPath, Description, Hosts, Size):
    with open('{}.txt'.format(ExportPath), 'w') as f:
        f.write("qq nfs_add_export --export-path /{} --fs-path /replicated/{} --description {} --create-fs-path --no-restrictions\n".format(ExportPath, ExportPath, Description))
        f.write("qq nfs_mod_export_host_access --export-path /{} modify_entry --position 1 --hosts {}\n".format(ExportPath, Hosts))
        f.write("qq quota_create_quota --path /replicated/{} --limit {}\n".format(ExportPath, Size))
        f.write("\n")

def main():
    ExportPath, Description, Hosts, Size = get_input()
    write_to_file(ExportPath, Description, Hosts, Size)

if __name__ == "__main__":
    main()