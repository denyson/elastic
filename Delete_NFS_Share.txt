# Delete NFS share and folder
qq nfs_delete_export --export-path /share3
qq tree_delete_create --path /share3

Double check size of the share to make sure it match and that you are deleting the correct share.
