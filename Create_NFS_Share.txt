# Create NFS share share3 Request 783646
qq nfs_add_export --export-path /share3 --fs-path /nonreplicated/share3 --description "Request 783646 John Doe doejohn@msu.edu" --create-fs-path --no-restrictions 
qq nfs_mod_export_host_access --export-path /share3 modify_entry --position 1 --hosts 35.8.3.1 35.8.3.2 10.1.42.0/24 
qq quota_create_quota --path /nonreplicated/share3 --limit 2TiB

