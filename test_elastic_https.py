import socket

def connect_to_ip(ip, port=443):
    """
    Connect to an IP address on a specified port.
    
    Args:
    ip (str): The IP address to connect to.
    port (int): The port number to use. Defaults to 443.
    
    Returns:
    bool: True if the connection is successful, False otherwise.
    """
    try:
        socket.create_connection((ip, port), timeout=5)
        return True
    except (socket.error, socket.timeout):
        return False


def main():
    # List of IP addresses to connect to
    ip_addresses = [
        "35.12.198.10",
        "35.12.198.11",
        "35.12.198.12",
        "35.12.198.13",
        "35.12.198.14",
        "35.12.198.15",
        "35.12.198.16",
        "35.12.198.17",
        "35.12.198.18",
        "35.12.198.19",
        "35.12.198.20",
        "35.12.198.21",
        "35.12.198.22",
        "35.12.198.23",
        "35.12.198.24",
        "35.12.198.25",
        "35.12.198.26",
        "35.12.198.27",
        "35.12.198.28",
        "35.12.198.29",
        "35.12.198.30",
        "35.12.198.31",
        "35.12.198.32",
        "35.12.198.33",
        "35.12.207.10",
        "35.12.207.11",
        "35.12.207.12",
        "35.12.207.13",
        "35.12.207.14",
        "35.12.207.15",
        "35.12.21.212",
        "35.12.21.213",
        "35.12.21.214",
        "35.12.21.215",
        "35.12.21.216",
        "35.12.21.217",
        "35.12.21.218",
        "35.12.21.219",
        "172.30.133.10",
        "172.30.133.11",
        "172.30.133.12",
        "172.30.133.13",
        "172.30.133.14",
        "172.30.133.15",
        "172.30.133.16",
        "172.30.133.17",
        "172.30.133.18",
        "172.30.133.19",
        "172.30.133.20",
        "172.30.133.21",
        "172.30.133.22",
        "172.30.133.23",
        "172.30.133.24",
        "172.30.133.25",
        "172.30.133.26",
        "172.30.133.27",
        "172.30.133.28",
        "172.30.133.29",
        "172.30.133.30",
        "172.30.133.31",
        "172.30.133.32",
        "172.30.133.33",
        "35.12.237.202",
        "35.12.237.203",
        "35.12.237.204",
        "35.12.237.205",
        "35.12.237.206",
        "35.12.237.207",
    ]

    # Initialize counters
    successful_connections = 0
    failed_connections = []

    # Iterate over the IP addresses
    for ip in ip_addresses:
        print(f"Connecting to {ip} on port 443...")
        if connect_to_ip(ip):
            print(f"Connected to {ip} successfully!\n")
            successful_connections += 1
        else:
            print(f"Failed to connect to {ip}.\n")
            failed_connections.append(ip)

    # Print summary
    print("Summary:")
    print(f"Successful connections: {successful_connections}")
    print(f"Failed connections: {len(failed_connections)}")
    print("Failed connections IPs:")
    for ip in failed_connections:
        print(ip)


if __name__ == "__main__":
    main()